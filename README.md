# Terraform Helloworld with flux-tf-controller

Here is a demo project for the tf-controller of FluxCD.

## Prepare

First install the last tf-controller version. See: 
<https://weaveworks.github.io/tf-controller/getting_started/#installation>

In this demo:

```yml
flux: v2.0.0-rc.5
helm-controller: v0.34.1
kustomize-controller: v1.0.0-rc.4
notification-controller: v1.0.0-rc.4
source-controller: v1.0.0-rc.5
tf-controller: v0.15.0-rc.4
```

Then add this gitrepo:

```sh
kubectl apply -f hello-repo.yml
```

## Greet Alice and Bob

```
kubectl apply -k hello-alice
kubectl apply -k hello-bob
```

What append ?

We ask the terraform-controller to use the terraform script `main.tf` with
a config map containing the name of alice and bob.

Let see the terraform status
```sh
kubectl get terraform
NAME          READY     STATUS                       AGE
hello-alice   Unknown   Initializing                 22m
hello-bob     Unknown   Reconciliation in progress   3s
```

## The issue: configmap dependency at deletion

At deletion:
```sh
kubectl delete -k hello-alice
configmap "hello-config-alice" deleted
terraform.infra.contrib.fluxcd.io "hello-alice" deleted
...
```

ConfigMap is deleted, but terraform deletion is stucked... because it needs
the related configmap:

```json
{"level":"error",
 "ts":"2023-06-28T13:43:41.993Z",
 "logger":"runner.terraform",
 "msg":"unable to get object key",
 "instance-id":"2b46e6cb-96bf-4df3-8758-48c94481dd2f",
 "objectKey":{
    "name":"hello-config-alice",
    "namespace":"flux-system"},
 "configmap":"",
 "error":"configmaps \"hello-config-alice\" not found"
}
```

Today, the only solution to avoid that is to remove first the terraform, wait
for it deletion, then delete the configmap :(.